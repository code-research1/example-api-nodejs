import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

import { responseCodes, responseMessages, ResponseConstant }  from '../../constants/responseConstant';
import { stringConstant }  from '../../constants/stringConstant';
import { formatTimestamp, genHMACSHA256 } from '../../utils/commonUtils';
import Config from '../../configuration/yamlConfig';

const environment = Config.environment;


export function authMiddleware(req: Request, res: Response, next: NextFunction) {
  // Get the JWT token from the request headers or cookies

    const token = req.headers.authorization?.split(' ')[1];
    const apiKey = req.headers['api-key'];
    const signature = req.headers['signature'];
    const signatureTime = req.headers['signature-time'];

    if(apiKey != Config.key[environment].apiKeyEncode) {

        const response: ResponseConstant = {
            responseCode: responseCodes.UNAUTHORIZED,
            responseDescription: responseMessages.ERROR,
            responseTime: formatTimestamp(Date.now()),
            responseDatas: stringConstant.HEADER_API_KEY,
        };

        return res.status(401).json(response);
    }

    if(signature != Config.key[environment].signatureKeyEncode) {
 
        const response: ResponseConstant = {
            responseCode: responseCodes.UNAUTHORIZED,
            responseDescription: responseMessages.ERROR,
            responseTime: formatTimestamp(Date.now()),
            responseDatas: stringConstant.HEADER_SIGNATURE_KEY,
        };

        return res.status(401).json(response);
    }

    if(Number(signatureTime) <= 0) {

        const response: ResponseConstant = {
            responseCode: responseCodes.UNAUTHORIZED,
            responseDescription: responseMessages.ERROR,
            responseTime: formatTimestamp(Date.now()),
            responseDatas: stringConstant.HEADER_API_KEY,
        };

        return res.status(401).json(response);
    }

    if (!token) {
        const response: ResponseConstant = {
            responseCode: responseCodes.UNAUTHORIZED,
            responseDescription: responseMessages.ERROR,
            responseTime: formatTimestamp(Date.now()),
            responseDatas: stringConstant.UNAUTHORIZED_ACCESS,
        };

        return res.status(401).json(response);
    }

    try {
        const signature = genHMACSHA256(Config.key[environment].signatureKey, Config.key[environment].apiKey);

        const decoded = jwt.verify(token, signature);

        // Store the decoded token data in the request for future use
        (req as any).user = decoded;

        next();
    } catch (error) {
        if (error.name === 'TokenExpiredError') {
            const response: ResponseConstant = {
                responseCode: responseCodes.UNAUTHORIZED,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: stringConstant.PROVIDED_TOKEN_EXPIRED,
            };
    
            return res.status(401).json(response);
        
        } else if (error.name === 'JsonWebTokenError') {
            const response: ResponseConstant = {
                responseCode: 403,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: stringConstant.TOKEN_DECODING,
            };
    
            return res.status(403).json(response);

        } else {
            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: stringConstant.INTERNAL_SERVER_ERROR,
            };
    
            return res.status(500).json(response);
        }
    }
}