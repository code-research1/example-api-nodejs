import { Router } from 'express';
import AuthController from '../controllers/authController';

class AuthRoutes {
    router = Router();
    authCtrl = new AuthController();

    constructor() {
        this.intializeRoutes();
    }

    intializeRoutes() {

        /**
         * @swagger
         * /login:
         *   post:
         *     summary: authenticate user
         *     description: Takes a create token. Return saved JSON.
         *     tags:
         *       - Authenticate user  
         *     consumes:
         *       - application/json
         *     produces:
         *       - application/json
         *     requestBody:
         *       description: JSON object containing resource information.
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               username:
         *                 type: string
         *               password:
         *                 type: string
         *           example:
         *             username: This is an Example Resource
         *             password: This is an Example Resource
         *     responses:
         *       200:
         *         description: Resource created successfully
         *       400:
         *         description: Bad request
         */
        this.router.route('/').post(this.authCtrl.checkLogin);
    }
}


export default new AuthRoutes().router;
