import { Router } from 'express';
import CommonController from '../controllers/commonController';

class CommonRoutes {
    router = Router();
    commonCtrl = new CommonController();

    constructor() {
        this.intializeRoutes();
    }

    intializeRoutes() {
        this.router.route('/encrypt').post(this.commonCtrl.encrypt);
        this.router.route('/decrypt').post(this.commonCtrl.decrypt);
    }
}


export default new CommonRoutes().router;
