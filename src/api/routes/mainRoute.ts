import express, { Application, Router } from 'express';
import Config from '../../configuration/yamlConfig';
import swaggerUi from 'swagger-ui-express';
import { swaggerSpec } from '../../swagger/swagger';

import authRoutes from './authRoute';
import commonRoutes from './commonRoute';
import roleRoutes from './roleRoute';
import userRoutes from './userRoute';

const router: Router = express.Router();

    router.get('/', (req, res) => {
        res.json("Hello, welcome to Web Service "+ Config.appName);
    });

    router.use('/docs', swaggerUi.serve);
    router.get('/docs', swaggerUi.setup(swaggerSpec));

    router.use('/login', authRoutes);
    router.use('/common', commonRoutes);
    router.use('/role', roleRoutes);
    router.use('/user', userRoutes);

export default router;