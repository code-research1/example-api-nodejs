import { Router } from 'express';
import RoleController from '../controllers/roleController';

import { authMiddleware } from '../middlewares/authMiddleware';

class RoleRoutes {
    router = Router();
    roleCtrl = new RoleController();

    constructor() {
        this.intializeRoutes();
    }

    intializeRoutes() {

        /**
         * @swagger
         * /role:
         *   get:
         *     summary: get all exists role
         *     description: Get all exists role.
         *     parameters:
         *       - in: header
         *         name: Api-Key
         *         required: true
         *         schema:
         *           type: string
         *         description: Api-Key.
         *       - in: header
         *         name: Signature
         *         required: true
         *         schema:
         *           type: string
         *         description: Signature.
         *       - in: header
         *         name: Signature-Time
         *         required: true
         *         schema:
         *           type: string
         *         description: Signature-Time.
         *     tags:
         *       - Role
         *     consumes:
         *       - application/json
         *     produces:
         *       - application/json
         *     responses:
         *       200:
         *         description: Resource retrieved successfully
         *       400:
         *         description: Bad request
         */
        this.router.route('/').get(authMiddleware, this.roleCtrl.findAllRoles);
        
        
        /**
         * @swagger
         * /role/getDataById/{idMasterRoles}:
         *   get:
         *     summary:  get one exists role
         *     description: Get one exists role.
         *     tags:
         *       - Role
         *     produces:
         *       - application/json
         *     parameters:
         *       - name: id
         *         in: path
         *         description: The unique ID of the data to retrieve.
         *         required: true
         *         schema:
         *           type: string
         *     responses:
         *       200:
         *         description: Data retrieved successfully
         *       404:
         *         description: Data not found
         */
        this.router.route('/getDataById/:idMasterRoles').get(authMiddleware, this.roleCtrl.getDataById);
        
        /**
         * @swagger
         * /role/create:
         *   post:
         *     summary: create Role
         *     description: create Role.
         *     tags:
         *       - Role
         *     consumes:
         *       - application/json
         *     produces:
         *       - application/json
         *     requestBody:
         *       description: JSON object containing resource information.
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *           properties:
         *             roleName:
         *               type: string
         *             description:
         *               type: string
         *             createdBy:
         *               type: string
         *           example:
         *             roleName: This is an Example Resource
         *             description: This is an Example Resource
         *             createdBy: This is an example resource.
         *     responses:
         *       200:
         *         description: Resource created successfully
         *       400:
         *         description: Bad request
         */
        this.router.route('/store').post(authMiddleware, this.roleCtrl.create);
        
        /**
         * @swagger
         * /role/update/{idMasterRoles}:
         *   put:
         *     summary: update Role
         *     description: update Role.
         *     tags:
         *       - Role
         *     consumes:
         *       - application/json
         *     produces:
         *       - application/json
        *     requestBody:
         *       description: JSON object containing resource information.
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *           properties:
         *             idMasterRoles:
         *               type: string
         *             roleName:
         *               type: string
         *             description:
         *               type: string
         *             updatedBy:
         *               type: string
         *           example:
         *             idMasterRoles: This is an Example Resource
         *             roleName: This is an Example Resource
         *             description: This is an Example Resource
         *             updatedBy: This is an example resource.
         *     responses:
         *       200:
         *         description: Resource update successfully
         *       400:
         *         description: Bad request
         */
        this.router.route('/update/:idMasterRoles').put(authMiddleware, this.roleCtrl.update);
        
        
        /**
         * @swagger
         * /role/updateIsActive/{idMasterRoles}:
         *   put:
         *     summary: update isActive Role
         *     description: update isActive Role.
         *     tags:
         *       - Role
         *     consumes:
         *       - application/json
         *     produces:
         *       - application/json
         *     requestBody:
         *       description: JSON object containing resource information.
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *           properties:
         *             idMasterRoles:
         *               type: string
         *             isActive:
         *               type: string
         *             updatedBy:
         *               type: string
         *           example:
         *             idMasterRoles: This is an Example Resource
         *             isActive: This is an Example Resource
         *             updatedBy: This is an example resource.
         *     responses:
         *       200:
         *         description: Resource delete successfully
         *       400:
         *         description: Bad request
         */
        this.router.route('/updateIsActive/:idMasterRoles').post(authMiddleware, this.roleCtrl.updateIsActive);
    }
}


export default new RoleRoutes().router;
