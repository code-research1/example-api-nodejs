import { Request, Response, NextFunction } from 'express';
import { v4 as uuidv4 } from 'uuid';

import UserRepository from '../repositories/userRepository';
import RoleRepository from '../repositories/roleRepository';

import { responseCodes, responseMessages, ResponseConstant, PaginateController }  from '../../constants/responseConstant';
import { formatTimestamp } from '../../utils/commonUtils';
import { stringConstant } from '../../constants/stringConstant';
import bcrypt from 'bcrypt';

export default class UserController {
    constructor() { }
  
    async findAll(req: Request, res: Response, next: NextFunction) {
        try {
            const { page, limit } = req.query;

            const response = await UserRepository.findAllUsers(req.query);

            const paginate: PaginateController = {
                pageNumber: page,
                pageSize: limit,
                totalRecordsCount: response.totalRecordsCount,
                records: response.records,
            };

            const result: ResponseConstant = {
                responseCode: responseCodes.SUCCESS,
                responseDescription: responseMessages.SUCCESS,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: paginate,
            };

            res.status(200).json(result);
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: "",
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async getAllDataUserByParam(req: Request, res: Response, next: NextFunction) {
        
        const key = req.body.key;
        const value = req.body.value;
        
        try {
            const datas = await UserRepository.getAllDataUserByParam(value);
            
            if (Array.isArray(datas) && datas.length === 0) {
             
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.ERROR,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: responseMessages.NOT_FOUND,
                };
                res.status(200).json(response);
            } else {
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: "",
                    responseDatas: datas,
                };
                res.status(200).json(response);
            }
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: "",
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async getDataById(req: Request, res: Response, next: NextFunction) {
        
        
        try {
            const datas = await UserRepository.getDataById(req.params.idMasterUsers);

            if (datas == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: datas,
                };
    
                res.status(200).json(response);
            }
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async create(req: Request, res: Response, next: NextFunction) {
        
        const { idMasterRoles
                , fullname
                , username
                , isGender
                , address
                , hpNumber
                , email
                , createdBy } = req.body;

        try {
            const StoreUserCriteria = {
                idMasterUsers: uuidv4(),
                idMasterRoles: idMasterRoles,
                fullname: fullname,
                username: username,
                isGender: isGender,
                address: address,
                hpNumber: hpNumber,
                password: await bcrypt.hash("p@ssw0rd", 10),
                email: email,
                createdBy: createdBy,
                isActive: 'ACTIVED',
            };

            const datas = await UserRepository.createUsers(StoreUserCriteria);

            const response: ResponseConstant = {
                responseCode: responseCodes.SUCCESS,
                responseDescription: responseMessages.SUCCESS,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: stringConstant.SUCCESSFULLY_ADD,
            };

            res.status(200).json(response);
           
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async update(req: Request, res: Response, next: NextFunction) {
        
        const { idMasterUsers } = req.params;
        const { idMasterRoles
                , fullname
                , username
                , isGender
                , address
                , hpNumber
                , email
                , updatedBy } = req.body;

        try {
            const UpdateUserCriteria = {
                idMasterUsers: idMasterUsers,
                idMasterRoles: idMasterRoles,
                fullname: fullname,
                username: username,
                isGender: isGender,
                address: address,
                hpNumber: hpNumber,
                email: email,
                updatedBy: updatedBy,
                isActive: 'ACTIVED',
            };

            const dataUsers = await UserRepository.getDataById(idMasterUsers);
            const dataRoles = await RoleRepository.getDataById(idMasterRoles);

            if (dataUsers == null && dataRoles == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{
                const datas = await UserRepository.updateUsers(idMasterUsers, UpdateUserCriteria);
    
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.SUCCESSFULLY_UPDATE,
                };
    
                res.status(200).json(response);
            }
           
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async updateIsActive(req: Request, res: Response, next: NextFunction) {
        
        const { idMasterUsers } = req.params;
        const { isActive, updatedBy } = req.body;

        try {
            const UpdateIsActiveUserCriteria = {
                idMasterUsers: idMasterUsers,
                isActive: isActive,
                updatedBy: updatedBy,
            };

            const datas = await UserRepository.getDataById(idMasterUsers);

            if (datas == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{
                const datas = await UserRepository.updateUsers(idMasterUsers, UpdateIsActiveUserCriteria);
    
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.SUCCESSFULLY_DELETE,
                };
    
                res.status(200).json(response);
            }
            
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }
  
}