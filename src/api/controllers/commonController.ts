import { Request, Response, NextFunction } from 'express';
import * as crypto from 'crypto';
import { responseCodes, responseMessages, ResponseConstant }  from '../../constants/responseConstant';
import { applicationConstant }  from '../../constants/applicationConstant';
import { stringConstant }  from '../../constants/stringConstant';

import { formatTimestamp } from '../../utils/commonUtils';

export default class CommonController {
    constructor() { }
  
    async encrypt(req: Request, res: Response, next: NextFunction) {
        
        
        try {
            const { data, keyType } = req.body;

            let constantKey = "";
            if(keyType === "DATA"){
                constantKey = Buffer.from(applicationConstant.KEY_AES, 'utf8').toString('hex');
            } else if(keyType === "PASSWORD"){
                constantKey = Buffer.from(applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
            } 

            const keyBuffer = Buffer.from(constantKey, 'hex');
            const iv = crypto.randomBytes(16); // 128-bit IV
            
            // Ensure data is a string (convert it if it's an object or other data type)
            const dataToEncrypt = typeof data === 'string' ? data : JSON.stringify(data);

            const cipher = crypto.createCipheriv('aes-256-cbc', keyBuffer, iv);
            
            let encryptedData = cipher.update(dataToEncrypt, 'utf8', 'hex');
            encryptedData += cipher.final('hex');
        
            // Concatenate IV and encrypted data
            const combinedData = iv.toString('hex') + encryptedData;
          

            const response: ResponseConstant = {
                responseCode: responseCodes.SUCCESS,
                responseDescription: responseMessages.SUCCESS,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: combinedData,
            };

            res.status(200).json(response);
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async decrypt(req: Request, res: Response, next: NextFunction) {
        
        
        try {
            const { encryptedData, keyType } = req.body;
            
            let constantKey = "";
            if(keyType === "DATA"){
                constantKey = Buffer.from(applicationConstant.KEY_AES, 'utf8').toString('hex');
            } else if(keyType === "PASSWORD"){
                constantKey = Buffer.from(applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
            } 
            
            // Extract IV and ciphertext
            const ivHex = encryptedData.slice(0, 32);
            const iv = Buffer.from(ivHex, 'hex');
            const ciphertext = encryptedData.slice(32);
        
            const keyBuffer = Buffer.from(constantKey, 'hex');
            const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
        
            let decrypted = decipher.update(ciphertext, 'hex', 'utf8');
            decrypted += decipher.final('utf8');
        
            let decryptedData;

            try {
                decryptedData = JSON.parse(decrypted);
            } catch (e) {
                decryptedData = decrypted;
            }

            const response: ResponseConstant = {
                responseCode: responseCodes.SUCCESS,
                responseDescription: responseMessages.SUCCESS,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: decryptedData,
            };

            res.status(200).json(response);
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }
  
}