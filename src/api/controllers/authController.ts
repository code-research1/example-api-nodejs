import { Request, Response, NextFunction } from 'express';
import * as bcrypt from 'bcrypt';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import * as uuid from 'uuid';

import AuthRepository from '../repositories/authRepository';
import { responseCodes, responseMessages, ResponseConstant }  from '../../constants/responseConstant';
import { stringConstant }  from '../../constants/stringConstant';
import { formatTimestamp, genHMACSHA256 } from '../../utils/commonUtils';
import Config from '../../configuration/yamlConfig';
import { applicationConstant } from '../../constants/applicationConstant';

const environment = Config.environment;

export default class AuthController {
    constructor() { }
  
    async checkLogin(req: Request, res: Response, next: NextFunction) {
        
        const { username, password } = req.body;

        try {
            const datas = await AuthRepository.findOne(username);

            if (datas == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{

                let constantKey = Buffer.from(applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
                
                
                // Extract IV and ciphertext
                const ivHex = password.slice(0, 32);
                const iv = Buffer.from(ivHex, 'hex');
                const ciphertext = password.slice(32);
            
                const keyBuffer = Buffer.from(constantKey, 'hex');
                const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
            
                let decrypted = decipher.update(ciphertext, 'hex', 'utf8');
                decrypted += decipher.final('utf8');
            
                let decryptedPass;

                try {
                    decryptedPass = JSON.parse(decrypted);
                } catch (e) {
                    decryptedPass = decrypted;
                }

                const passwordIsValid = await bcrypt.compareSync(decryptedPass, datas.password);
                if (!passwordIsValid) {
                    const response: ResponseConstant = {
                        responseCode: responseCodes.UNAUTHORIZED,
                        responseDescription: responseMessages.ERROR,
                        responseTime: formatTimestamp(Date.now()),
                        responseDatas: stringConstant.INVALID_PASSWORD,
                    };
        
                    res.status(401).json(response);
                }

                const payload = {
                    iss: Config.key[environment].apiKeyEncode,
                    iat: Math.floor(Date.now() / 1000),
                    exp: Math.floor(Date.now() / 1000) +  86400 * 5, 
                    authorized: true,
                    jti: uuid.v4(), 
                };

                const signatureKey = Buffer.from(Config.key[environment].signatureKeyEncode, 'base64').toString('utf-8');
                const apiKey = Buffer.from(Config.key[environment].apiKeyEncode, 'base64').toString('utf-8');

                const signature = genHMACSHA256(signatureKey, apiKey);

                const token = jwt.sign(payload,
                    signature,
                    {
                     algorithm: 'HS256',
                    });

                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: token,
                };
    
                res.status(200).json(response);

            }
           
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }
  
}