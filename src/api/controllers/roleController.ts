import { Request, Response, NextFunction } from 'express';
import { v4 as uuidv4 } from 'uuid';

import RoleRepository from './../repositories/roleRepository';

import { responseCodes, responseMessages, ResponseConstant, PaginateController }  from '../../constants/responseConstant';
import { formatTimestamp } from '../../utils/commonUtils';
import { stringConstant } from '../../constants/stringConstant';

export default class RoleController {
    constructor() { }
  
    async findAllRoles(req: Request, res: Response, next: NextFunction) {
        
        try {
            const { page, limit } = req.query;

            const response = await RoleRepository.findAllRoles(req.query);

            const paginate: PaginateController = {
                pageNumber: page,
                pageSize: limit,
                totalRecordsCount: response.totalRecordsCount,
                records: response.records,
            };

            const result: ResponseConstant = {
                responseCode: responseCodes.SUCCESS,
                responseDescription: responseMessages.SUCCESS,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: paginate,
            };

            res.status(200).json(result);
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async getDataById(req: Request, res: Response, next: NextFunction) {
        
        try {
            const datas = await RoleRepository.getDataById(req.params.idMasterRoles);

            if (datas == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: datas,
                };
    
                res.status(200).json(response);
            }
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async create(req: Request, res: Response, next: NextFunction) {
        
        const { roleName, description, createdBy } = req.body;

        try {
            const StoreRoleCriteria = {
                idMasterRoles: uuidv4(),
                roleName: roleName,
                description: description,
                createdBy: createdBy,
                isActive: 'ACTIVED',
            };

            const datas = await RoleRepository.createRoles(StoreRoleCriteria);

            const response: ResponseConstant = {
                responseCode: responseCodes.SUCCESS,
                responseDescription: responseMessages.SUCCESS,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: stringConstant.SUCCESSFULLY_ADD,
            };

            res.status(200).json(response);
           
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async update(req: Request, res: Response, next: NextFunction) {
        
        const { idMasterRoles } = req.params;
        const { roleName, description, updatedBy } = req.body;

        try {
            const UpdateRoleCriteria = {
                idMasterRoles: idMasterRoles,
                roleName: roleName,
                description: description,
                updatedBy: updatedBy,
            };

            const datas = await RoleRepository.getDataById(idMasterRoles);

            if (datas == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{
                const datas = await RoleRepository.updateRoles(idMasterRoles, UpdateRoleCriteria);
    
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.SUCCESSFULLY_UPDATE,
                };
    
                res.status(200).json(response);
            }
           
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }

    async updateIsActive(req: Request, res: Response, next: NextFunction) {
        

        const { idMasterRoles } = req.params;
        const { isActive, updatedBy } = req.body;

        try {
            const UpdateIsActiveRoleCriteria = {
                idMasterRoles: idMasterRoles,
                isActive: isActive,
                updatedBy: updatedBy,
            };

            const datas = await RoleRepository.getDataById(idMasterRoles);

            if (datas == null) {
                const response: ResponseConstant = {
                    responseCode: responseCodes.NOT_FOUND,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.DATA_NOT_FOUND,
                };
    
                res.status(404).json(response);
            } else{
                const datas = await RoleRepository.updateRoles(idMasterRoles, UpdateIsActiveRoleCriteria);
    
                const response: ResponseConstant = {
                    responseCode: responseCodes.SUCCESS,
                    responseDescription: responseMessages.SUCCESS,
                    responseTime: formatTimestamp(Date.now()),
                    responseDatas: stringConstant.SUCCESSFULLY_DELETE,
                };
    
                res.status(200).json(response);
            }
           
        } catch (error) {

            const response: ResponseConstant = {
                responseCode: responseCodes.ERROR,
                responseDescription: responseMessages.ERROR,
                responseTime: formatTimestamp(Date.now()),
                responseDatas: error,
            };
          
            res.status(500).send(response);
        }
    }
  
}