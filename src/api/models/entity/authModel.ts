import { Model, DataTypes } from 'sequelize';
import { sequelize } from '../../../configuration/databaseConfig';

export class AuthModel extends Model {
    public idMasterUsers!: number;
    public username!: string;
    public password!: string;
    public isActive!: string;
    public createdAt!: Date;
    public updatedAt!: Date;
}
  
AuthModel.init(
    {
        idMasterUsers: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            unique: true,
            field: 'id_master_users'
        },
        username: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'username'
        },
        password: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'password'
        },
        isActive: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'is_active'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'created_at',
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'updated_at',
        },
    },
    {
      sequelize,
      tableName: 'master_users',
    },
);