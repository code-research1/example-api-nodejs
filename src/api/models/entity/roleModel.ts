import { Model, DataTypes } from 'sequelize';
import { sequelize } from '../../../configuration/databaseConfig';

export class RoleModel extends Model {
    public idMasterRoles!: number;
    public roleName!: string;
    public description!: string;
    public createdBy!: string;
    public updatedBy!: string;
    public isActive!: string;
    public createdAt!: Date;
    public updatedAt!: Date;
}
  
RoleModel.init(
    {
        idMasterRoles: { 
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            unique: true,
            field: 'id_master_roles'
        },
        roleName: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'role_name'
        },
        description: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'description'
        },
        createdBy: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'created_by'
        },
        updatedBy: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'updated_by'
        },
        isActive: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'is_active'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'created_at',
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'updated_at',
        },
    },
    {
      sequelize,
      tableName: 'master_roles',
    },
);
