import { Model, DataTypes } from 'sequelize';
import { sequelize } from '../../../configuration/databaseConfig';
import { RoleModel } from './roleModel';

export class UserModel extends Model {
    public idMasterUsers!: number;
    public idMasterRoles!: string;
    public fullname!: string;
    public username!: string;
    public isGender!: string;
    public address!: string;
    public hpNumber!: string;
    public dateActivation!: Date;
    public email!: string;
    public emailVerifiedAt!: Date;
    public urlPhoto!: string;
    public createdBy!: string;
    public updatedBy!: string;
    public isActive!: string;
    public createdAt!: Date;
    public updatedAt!: Date;
}
  
UserModel.init(
    {
        idMasterUsers: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            unique: true,
            field: 'id_master_users'
        },
        idMasterRoles: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            field: 'id_master_roles'
        },
        fullname: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'fullname'
        },
        username: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'username'
        },
        isGender: {
            type: DataTypes.STRING(1),
            allowNull: false,
            field: 'is_gender'
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: true,
            field: 'address'
        },
        hpNumber: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'hp_number'
        },
        dateActivation: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'date_activation'
        },
        email: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'email'
        },
        emailVerifiedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'email_verified_at'
        },
        password: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'password'
        },
        urlPhoto: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'url_photo'
        },
        createdBy: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'created_by'
        },
        updatedBy: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'updated_by'
        },
        isActive: {
            type: DataTypes.STRING(20),
            allowNull: true,
            field: 'is_active'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'created_at',
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'updated_at',
        },
    },
    {
      sequelize,
      tableName: 'master_users',
    },
);


UserModel.belongsTo(RoleModel, 
    { 
        foreignKey: 'idMasterRoles', as: 'role' 
    }
);