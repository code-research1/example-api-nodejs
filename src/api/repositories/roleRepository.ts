import { Op, Sequelize } from 'sequelize';
import { applicationConstant } from '../../constants/applicationConstant';
import { PaginateRepository } from '../../constants/responseConstant';
import { RoleModel } from '../models/entity/roleModel';


interface IRoleRepository {
    findAllRoles(req : any): Promise<PaginateRepository>;
    createRoles(datas: any): Promise<RoleModel>;
    updateRoles(id: String, datas: any): Promise<number>;
    getDataById(id: String): Promise<RoleModel | null>;
}

class RoleRepository implements IRoleRepository{

    constructor() { }

    async findAllRoles(req : any): Promise<PaginateRepository> {
        try {
            let searchCriteria: any = {};

            let page = parseInt(req.page, 10) || applicationConstant.FIRST_PAGE;
            let limit = parseInt(req.limit, 10) || applicationConstant.PAGINATION_MIN_LIMIT; 
            let sortDirection = req.sort_direction || applicationConstant.SORT_DESC;
            let sortBy = req.sort_by || applicationConstant.DEFAULT_SORT_COLOUMN;
            let searchBy = req.search_by;
            let search = req.search;
            let status = req.status;

            if (searchBy.length > 0 && search.length > 0) {
                if(searchBy == "role_name") {
                    searchCriteria.roleName = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
                }
            }

            if (status.length > 0) {
                searchCriteria.isActive = status;
            }

            const result = await RoleModel.findAndCountAll({ 
                where : searchCriteria,
                order: [[sortBy, sortDirection.toUpperCase()]],
                offset: (page-1) * limit, 
                limit: limit 
            });

            const response: PaginateRepository = {
                records: result.rows,
                totalRecordsCount: result.count,
            };

            return response;
        } catch (error) {
            throw new Error("Failed to retrieve Roles!");
        }
    }
    
    async createRoles(datas: any): Promise<RoleModel>{
        try {
            return await RoleModel.create(datas);
        } catch (err) {
            throw new Error("Failed to create Role!");
        }
    }

    async updateRoles(id: String, datas: any): Promise<number> {
        try {
            const affectedRows = await RoleModel.update(datas, { where: { idMasterRoles: id.toString() } });
            return affectedRows[0];
        } catch (error) {
            throw new Error("Failed to update Role!");
        }
    }

    async getDataById(id: String): Promise<RoleModel | null> {
        try {
            return await RoleModel.findByPk(id.toString());
        } catch (error) {
            throw new Error("Failed to retrieve Role!");
        }
    }

}

export default new RoleRepository();