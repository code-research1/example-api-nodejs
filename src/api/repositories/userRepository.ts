import { UserModel } from '../models/entity/userModel';
import { RoleModel } from '../models/entity/roleModel';
import { applicationConstant } from '../../constants/applicationConstant';
import { Op, Sequelize } from 'sequelize';
import { PaginateRepository } from '../../constants/responseConstant';

class UserRepository {

    constructor() { }

    async findAllUsers(req: any) {
        let searchCriteria: any = {};

        let page = parseInt(req.page, 10) || applicationConstant.FIRST_PAGE;
        let limit = parseInt(req.limit, 10) || applicationConstant.PAGINATION_MIN_LIMIT; 
        let sortDirection = req.sort_direction || applicationConstant.SORT_DESC;
        let sortBy = req.sort_by || applicationConstant.DEFAULT_SORT_COLOUMN;
        let searchBy = req.search_by;
        let search = req.search;
        let status = req.status;

        if (searchBy.length > 0 && search.length > 0) {
            if(searchBy == "fullname") {
                searchCriteria.fullname = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }

            if(searchBy == "address") {
                searchCriteria.address = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }

            if(searchBy == "username") {
                searchCriteria.username = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }
        }
        
        if (status.length > 0) {
            searchCriteria.isActive = status;
        }

        const result = await UserModel.findAndCountAll(
            {
                include: [
                    {
                        model: RoleModel,
                        as: 'role', 
                    }
                ],
                attributes: { exclude: ['password'] },
                where : searchCriteria,
                order: [[sortBy, sortDirection.toUpperCase()]],
                offset: (page-1) * limit, 
                limit: limit 
            }
        );

        const response: PaginateRepository = {
            records: result.rows,
            totalRecordsCount: result.count,
        };

        return response;
    }

    async getAllDataUserByParam(param: string) {
        return await UserModel.findAll({
            where: {
                email: param, 
            },
            attributes: { exclude: ['password'] },
        });
    }

    async createUsers(datas: any) {
        return await UserModel.create(datas);
    }

    async updateUsers(id: String, datas: any) {
        return await UserModel.update(datas, { where: { idMasterUsers: id.toString() } });
    }

    async getDataById(id: String) {
        return await UserModel.findByPk(id.toString(), {
            attributes: { exclude: ['password', 'age'] },
          });
    }
}

export default new UserRepository();