import { AuthModel } from '../models/entity/authModel';

class AuthRepository {

    constructor() { }

    findOne(param: string) {
        return AuthModel.findOne({
            where: {
                username: param, 
                isActive: "ACTIVED"
            },
        });
    }
    

}

export default new AuthRepository();