import express from 'express';
import { Application } from 'express';


import Server from './configuration/mainConfig';
import Config from './configuration/yamlConfig';

import {stringConstant} from '../src/constants/stringConstant';
import {createLog} from '../src/utils/commonUtils';

const app: Application = express();
const server: Server = new Server(app);


app.listen(Config.serverPort, 'localhost', function () {

  createLog(
		stringConstant.INFO,
		stringConstant.INFO,
		`Server is running on port ${Config.serverPort}`,
		200,
		"",
		0,
		null,
	);

}).on('error', (err: any) => {
  if (err.code === 'EADDRINUSE') {
    createLog(
			stringConstant.ERROR,
			stringConstant.ERROR,
			"server startup error: address already in use",
			500,
			"",
			err.lineNumber,
			err,
		);
    
  } else {
    createLog(
			stringConstant.ERROR,
			stringConstant.ERROR,
			"server failed to start",
			500,
			"",
			err.lineNumber,
			err,
		);
  }
});