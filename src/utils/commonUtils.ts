import * as crypto from 'crypto';
import { applicationConstant }  from '../constants/applicationConstant';
import { stringConstant }  from '../constants/stringConstant';
import logger from './loggerUtils';

const constantKey = Buffer.from(applicationConstant.KEY_AES, 'utf8').toString('hex');

export function formatTimestamp(timestampInMillis:any) {
    const date = new Date(timestampInMillis);
  
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
  
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

export function getLineNumber(error: unknown): number | null  {
    if (error instanceof Error) {
        const stackTrace = error.stack;
        if (stackTrace) {
            const stackLines = stackTrace.split('\n');
            if (stackLines.length >= 2) {
                    const lineMatch = /:(\d+):(\d+)/.exec(stackLines[1]);
                if (lineMatch) {
                    const lineNumber = parseInt(lineMatch[1], 10);
                return lineNumber;
                }
            }
        }
    }
    return null;
}


export function genHMACSHA256(message: string, secretKey: string) {
    const hmac = crypto.createHmac('sha256', secretKey);
    hmac.update(message);
    return hmac.digest('hex');
}

// Function to encrypt data using AES-256-CBC
export function encrypt(text: string) {

    const keyBuffer = Buffer.from(constantKey, 'hex');
    const iv = crypto.randomBytes(16); // 128-bit IV
    const cipher = crypto.createCipheriv('aes-256-cbc', keyBuffer, iv);

    let encryptedData = cipher.update(text, 'utf8', 'hex');
    encryptedData += cipher.final('hex');

    const combinedData = iv.toString('hex') + encryptedData;

	return combinedData;
}

// Function to decrypt data using AES-256-CBC
export function decrypt(encryptedText: string) {
    // Extract IV and ciphertext
    const ivHex = encryptedText.slice(0, 32);
    const iv = Buffer.from(ivHex, 'hex');
    const ciphertext = encryptedText.slice(32);

    const keyBuffer = Buffer.from(constantKey, 'hex');
    const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);

    let decryptedData = decipher.update(ciphertext, 'hex', 'utf8');
    decryptedData += decipher.final('utf8');

	return decryptedData;
}

export function createLog(logFlagging:string, title:string, message:string, code:number, endpoint:string, linenumber:number, datas:any) {
    
    if (logFlagging === stringConstant.SUCCESS) {
        logger.info({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    } else if (logFlagging === stringConstant.INFO) {
        logger.info({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    } else if (logFlagging === stringConstant.ERROR) {
        logger.error({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    } else {
        logger.error('logFlagging No match found');
    }
}