import winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

import Config from '../configuration/yamlConfig';
const environment = Config.environment;

// Define a custom log format
const customFormat = winston.format.printf(({ level, timestamp, message, title, code, endpoint, linenumber, datas }) => {
	return JSON.stringify({
	  'log.level': level,
	  '@timestamp': timestamp,
	  message,
	  title,
	  code,
	  endpoint,
	  linenumber,
	  datas,
	});
});

const logger = winston.createLogger({
	format: winston.format.combine(
		winston.format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ssZ' }),
		customFormat
	),
	transports: [
		new winston.transports.Console(),
		new DailyRotateFile({
			filename: Config.logDirectory[environment].path,
			datePattern: 'DD-MMM-YYYY',
			format: winston.format.combine(winston.format.uncolorize()),
		}),
	],
});

export default logger;