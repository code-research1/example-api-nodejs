import { Sequelize } from 'sequelize'; 

import Config from './yamlConfig';
import {decrypt} from '../utils/commonUtils';

const environment = Config.environment;

export const sequelize: Sequelize = new Sequelize(decrypt(Config.database[environment].connection)
                                , decrypt(Config.database[environment].username)
                                , decrypt(Config.database[environment].password), {
    host: decrypt(Config.database[environment].url),
    dialect: 'postgres',
    schema: decrypt(Config.database[environment].schema),
    port: Number(decrypt(Config.database[environment].port.toString())),
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

async function checkDatabaseConnection() {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully..');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

checkDatabaseConnection();
  