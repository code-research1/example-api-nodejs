import * as fs from 'fs';
import * as yaml from 'js-yaml';

interface LogDirConfig {
  path: string;
}

interface DatabaseConfig {
  connection: string;
  username: string;
  password: string;
  url: string;
  port: number;
  schema: string;
}

interface KeyConfig {
  apiKey: string;
  apiKeyEncode: string;
  signatureKey: string;
  signatureKeyEncode: string;
}

interface RouteConfig {
  name: string;
}

interface AppConfig {
  appName: string;
  serverPort: number;
  environment: string;
  releaseMode: string;
  logDirectory: {
    [key: string]: LogDirConfig;
  };
  database: {
    [key: string]: DatabaseConfig;
  };
  key: {
    [key: string]: KeyConfig;
  };
  route: {
    [key: string]: RouteConfig;
  };
}

const loadConfig = (filePath: string): AppConfig => {
  try {
    const fileContents = fs.readFileSync(filePath, 'utf8');
    return yaml.load(fileContents) as AppConfig;
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(`Error loading configuration: ${error.message}`);
    } else {
      throw new Error('An unknown error occurred while loading the configuration.');
    }
  }
};

const configPath = './src/config.yml';

const config: AppConfig = loadConfig(configPath);

export default config;