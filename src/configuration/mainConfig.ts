import { Application, urlencoded, json } from 'express';
import * as fs from 'fs';
import { WriteStream } from 'fs';
import * as path from 'path';
import helmet from 'helmet';
import * as winston from 'winston';

import rateLimiter from '../api/middlewares/rateLimitMiddleware';
import { corsMiddleware } from '../api/middlewares/corsMiddleware'; 
// import { unCoughtErrorHandler } from './handlers/errorHandler';
import Routes from '../api/routes/mainRoute';
import Config from '../configuration/yamlConfig';

const environment = Config.environment;

export default class Server {
    constructor(app: Application) {
        this.config(app)
    }

    public config(app : Application): void {
        // const accessLogStream: WriteStream = fs.createWriteStream(
        //     path.join(__dirname, './logs/access.log'),
        //     { flags: 'a' }
        // );
        // app.use(morgan('combined', { stream: accessLogStream }));
        app.use(urlencoded({ extended: true }));
        app.use(json());
        app.use(helmet());
        app.use(corsMiddleware); 
        app.use(Config.route[environment].name, Routes);
        app.use(rateLimiter()); //  apply to all requests
        // app.use(unCoughtErrorHandler);
    }
}

process.on('beforeExit', function (err) {
    winston.error(JSON.stringify(err));
    console.error(err);
});