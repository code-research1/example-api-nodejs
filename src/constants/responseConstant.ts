
export const responseCodes = {
    SUCCESS: 200,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    ERROR: 500,
};

export const responseMessages = {
    SUCCESS: "success",
    FAILED: "failed",
    ERROR: "error",
    INFO: "info",
    NOT_FOUND: 'Not Found',
    
};

export interface ResponseConstant {
    responseCode: number;
    responseDescription: string;
    responseTime: string;
    responseDatas: any; 
}

export interface PaginateController {
    pageNumber: any;
    pageSize: any;
    totalRecordsCount: number;
    records: any; 
}

export interface PaginateRepository {
    records: any; 
    totalRecordsCount: number;
}