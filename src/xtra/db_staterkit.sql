/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 140005 (140005)
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : db_staterkit

 Target Server Type    : PostgreSQL
 Target Server Version : 140005 (140005)
 File Encoding         : 65001

 Date: 21/10/2023 04:58:39
*/


-- ----------------------------
-- Table structure for master_roles
-- ----------------------------
DROP TABLE IF EXISTS "db_staterkit"."master_roles";
CREATE TABLE "db_staterkit"."master_roles" (
  "id_master_roles" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "role_name" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(191) COLLATE "pg_catalog"."default",
  "created_by" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "updated_by" varchar(191) COLLATE "pg_catalog"."default",
  "is_active" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0) NOT NULL,
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of master_roles
-- ----------------------------
INSERT INTO "db_staterkit"."master_roles" VALUES ('fbb5042c-7ddb-4c62-8fd0-2ccc9674e841', 'Superadmin', 'Superadmin', 'SYSTEM', '', 'ACTIVED', '2023-10-18 04:52:01', '2023-10-18 04:52:01');
INSERT INTO "db_staterkit"."master_roles" VALUES ('c494e061-90a4-4835-a25e-8f934361495e', 'Developer', 'Developer', 'SYSTEM', 'bayuwidia', 'ACTIVED', '2020-09-15 06:06:42', '2021-07-18 00:59:44');

-- ----------------------------
-- Table structure for master_users
-- ----------------------------
DROP TABLE IF EXISTS "db_staterkit"."master_users";
CREATE TABLE "db_staterkit"."master_users" (
  "id_master_users" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "id_master_roles" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "fullname" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "username" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "is_gender" varchar(1) COLLATE "pg_catalog"."default" NOT NULL,
  "address" text COLLATE "pg_catalog"."default",
  "hp_number" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "date_activation" timestamp(0),
  "email" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "email_verified_at" timestamp(0),
  "password" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "url_photo" varchar(191) COLLATE "pg_catalog"."default",
  "created_by" varchar(191) COLLATE "pg_catalog"."default" NOT NULL,
  "updated_by" varchar(191) COLLATE "pg_catalog"."default",
  "is_active" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0) NOT NULL,
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of master_users
-- ----------------------------
INSERT INTO "db_staterkit"."master_users" VALUES ('f6895106-3b54-40db-8712-91dda744029b', 'fbb5042c-7ddb-4c62-8fd0-2ccc9674e841', 'Superadmin', 'superadmin', 'L', 'Jakarta', '081234567890', '2023-10-21 04:58:01', 'superadmin@gmail.com', '2023-10-21 04:58:08', '$2a$10$EuXvsCws9GKL6JTOujpXy.GI51WvYYxFED0HFAZsyXnY/1tplyAxu', '', 'SYSTEM', '', 'ACTIVED', '2023-10-18 04:53:42', '2023-10-18 04:53:42');

-- ----------------------------
-- Primary Key structure for table master_roles
-- ----------------------------
ALTER TABLE "db_staterkit"."master_roles" ADD CONSTRAINT "master_roles_pkey" PRIMARY KEY ("id_master_roles");

-- ----------------------------
-- Uniques structure for table master_users
-- ----------------------------
ALTER TABLE "db_staterkit"."master_users" ADD CONSTRAINT "master_users_email_unique" UNIQUE ("email");
ALTER TABLE "db_staterkit"."master_users" ADD CONSTRAINT "master_users_username_unique" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table master_users
-- ----------------------------
ALTER TABLE "db_staterkit"."master_users" ADD CONSTRAINT "master_users_pkey" PRIMARY KEY ("id_master_users");
