"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const helmet_1 = __importDefault(require("helmet"));
const winston = __importStar(require("winston"));
const rateLimitMiddleware_1 = __importDefault(require("../api/middlewares/rateLimitMiddleware"));
const corsMiddleware_1 = require("../api/middlewares/corsMiddleware");
// import { unCoughtErrorHandler } from './handlers/errorHandler';
const mainRoute_1 = __importDefault(require("../api/routes/mainRoute"));
const yamlConfig_1 = __importDefault(require("../configuration/yamlConfig"));
const environment = yamlConfig_1.default.environment;
class Server {
    constructor(app) {
        this.config(app);
    }
    config(app) {
        // const accessLogStream: WriteStream = fs.createWriteStream(
        //     path.join(__dirname, './logs/access.log'),
        //     { flags: 'a' }
        // );
        // app.use(morgan('combined', { stream: accessLogStream }));
        app.use((0, express_1.urlencoded)({ extended: true }));
        app.use((0, express_1.json)());
        app.use((0, helmet_1.default)());
        app.use(corsMiddleware_1.corsMiddleware);
        app.use(yamlConfig_1.default.route[environment].name, mainRoute_1.default);
        app.use((0, rateLimitMiddleware_1.default)()); //  apply to all requests
        // app.use(unCoughtErrorHandler);
    }
}
exports.default = Server;
process.on('beforeExit', function (err) {
    winston.error(JSON.stringify(err));
    console.error(err);
});
