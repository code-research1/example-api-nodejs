"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sequelize = void 0;
const sequelize_1 = require("sequelize");
const yamlConfig_1 = __importDefault(require("./yamlConfig"));
const commonUtils_1 = require("../utils/commonUtils");
const environment = yamlConfig_1.default.environment;
exports.sequelize = new sequelize_1.Sequelize((0, commonUtils_1.decrypt)(yamlConfig_1.default.database[environment].connection), (0, commonUtils_1.decrypt)(yamlConfig_1.default.database[environment].username), (0, commonUtils_1.decrypt)(yamlConfig_1.default.database[environment].password), {
    host: (0, commonUtils_1.decrypt)(yamlConfig_1.default.database[environment].url),
    dialect: 'postgres',
    schema: (0, commonUtils_1.decrypt)(yamlConfig_1.default.database[environment].schema),
    port: Number((0, commonUtils_1.decrypt)(yamlConfig_1.default.database[environment].port.toString())),
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});
function checkDatabaseConnection() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield exports.sequelize.authenticate();
            console.log('Connection has been established successfully..');
        }
        catch (error) {
            console.error('Unable to connect to the database:', error);
        }
    });
}
checkDatabaseConnection();
