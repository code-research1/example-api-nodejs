"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createLog = exports.decrypt = exports.encrypt = exports.genHMACSHA256 = exports.getLineNumber = exports.formatTimestamp = void 0;
const crypto = __importStar(require("crypto"));
const applicationConstant_1 = require("../constants/applicationConstant");
const stringConstant_1 = require("../constants/stringConstant");
const loggerUtils_1 = __importDefault(require("./loggerUtils"));
const constantKey = Buffer.from(applicationConstant_1.applicationConstant.KEY_AES, 'utf8').toString('hex');
function formatTimestamp(timestampInMillis) {
    const date = new Date(timestampInMillis);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}
exports.formatTimestamp = formatTimestamp;
function getLineNumber(error) {
    if (error instanceof Error) {
        const stackTrace = error.stack;
        if (stackTrace) {
            const stackLines = stackTrace.split('\n');
            if (stackLines.length >= 2) {
                const lineMatch = /:(\d+):(\d+)/.exec(stackLines[1]);
                if (lineMatch) {
                    const lineNumber = parseInt(lineMatch[1], 10);
                    return lineNumber;
                }
            }
        }
    }
    return null;
}
exports.getLineNumber = getLineNumber;
function genHMACSHA256(message, secretKey) {
    const hmac = crypto.createHmac('sha256', secretKey);
    hmac.update(message);
    return hmac.digest('hex');
}
exports.genHMACSHA256 = genHMACSHA256;
// Function to encrypt data using AES-256-CBC
function encrypt(text) {
    const keyBuffer = Buffer.from(constantKey, 'hex');
    const iv = crypto.randomBytes(16); // 128-bit IV
    const cipher = crypto.createCipheriv('aes-256-cbc', keyBuffer, iv);
    let encryptedData = cipher.update(text, 'utf8', 'hex');
    encryptedData += cipher.final('hex');
    const combinedData = iv.toString('hex') + encryptedData;
    return combinedData;
}
exports.encrypt = encrypt;
// Function to decrypt data using AES-256-CBC
function decrypt(encryptedText) {
    // Extract IV and ciphertext
    const ivHex = encryptedText.slice(0, 32);
    const iv = Buffer.from(ivHex, 'hex');
    const ciphertext = encryptedText.slice(32);
    const keyBuffer = Buffer.from(constantKey, 'hex');
    const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
    let decryptedData = decipher.update(ciphertext, 'hex', 'utf8');
    decryptedData += decipher.final('utf8');
    return decryptedData;
}
exports.decrypt = decrypt;
function createLog(logFlagging, title, message, code, endpoint, linenumber, datas) {
    if (logFlagging === stringConstant_1.stringConstant.SUCCESS) {
        loggerUtils_1.default.info({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    }
    else if (logFlagging === stringConstant_1.stringConstant.INFO) {
        loggerUtils_1.default.info({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    }
    else if (logFlagging === stringConstant_1.stringConstant.ERROR) {
        loggerUtils_1.default.error({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    }
    else {
        loggerUtils_1.default.error('logFlagging No match found');
    }
}
exports.createLog = createLog;
