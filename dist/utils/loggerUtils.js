"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = __importDefault(require("winston"));
const winston_daily_rotate_file_1 = __importDefault(require("winston-daily-rotate-file"));
const yamlConfig_1 = __importDefault(require("../configuration/yamlConfig"));
const environment = yamlConfig_1.default.environment;
// Define a custom log format
const customFormat = winston_1.default.format.printf(({ level, timestamp, message, title, code, endpoint, linenumber, datas }) => {
    return JSON.stringify({
        'log.level': level,
        '@timestamp': timestamp,
        message,
        title,
        code,
        endpoint,
        linenumber,
        datas,
    });
});
const logger = winston_1.default.createLogger({
    format: winston_1.default.format.combine(winston_1.default.format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ssZ' }), customFormat),
    transports: [
        new winston_1.default.transports.Console(),
        new winston_daily_rotate_file_1.default({
            filename: yamlConfig_1.default.logDirectory[environment].path,
            datePattern: 'DD-MMM-YYYY',
            format: winston_1.default.format.combine(winston_1.default.format.uncolorize()),
        }),
    ],
});
exports.default = logger;
