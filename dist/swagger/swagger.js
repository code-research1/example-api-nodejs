"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.swaggerSpec = void 0;
const swagger_jsdoc_1 = __importDefault(require("swagger-jsdoc"));
const url = '/dev/api/v1/example-api-express';
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Express API With Typescript Clean Architecture',
            version: '1.0.0',
            description: 'API documentation for an Express.js app with Swagger',
            termsOfService: 'https://www.example.com/terms-of-service',
            contact: {
                name: 'API Support',
                email: 'support@swagger.io',
                url: 'http://www.apache.org/licenses/LICENSE-2.0.html', // Include a URL for contact information if applicable
            },
        },
        servers: [
            {
                url: 'http://localhost:3000/dev/api/v1/example-api-nodejs', // Update with your server URL
            },
        ],
        components: {
            securitySchemes: {
                apiKey: {
                    type: 'apiKey',
                    name: 'Authorization',
                    in: 'header',
                },
                // Define other security schemes as needed
            },
        },
    },
    // List of files to be processed for Swagger documentation
    apis: ['./src/api/routes/*.ts'], // Path to your API route files
};
const swaggerSpec = (0, swagger_jsdoc_1.default)(options);
exports.swaggerSpec = swaggerSpec;
