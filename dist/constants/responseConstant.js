"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.responseMessages = exports.responseCodes = void 0;
exports.responseCodes = {
    SUCCESS: 200,
    UNAUTHORIZED: 401,
    NOT_FOUND: 404,
    ERROR: 500,
};
exports.responseMessages = {
    SUCCESS: "success",
    FAILED: "failed",
    ERROR: "error",
    INFO: "info",
    NOT_FOUND: 'Not Found',
};
