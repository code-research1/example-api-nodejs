"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringConstant = void 0;
exports.stringConstant = {
    HEADER_API_KEY: "Header Api-Key not match.",
    HEADER_SIGNATURE_KEY: "Header Signature not match.",
    HEADER_SIGNATURE_TIME: "Header Signature-Time not match.",
    UNAUTHORIZED_ACCESS: "Unauthorized access, you do not have authorized access to this function",
    DATA_NOT_FOUND: "Not found data record",
    INVALID_PASSWORD: "Invalid Password",
    DUPLICATE_DATA: "Data duplication",
    PROVIDED_TOKEN_EXPIRED: "Provided token is expired",
    TOKEN_DECODING: "An error while decoding token",
    INTERNAL_SERVER_ERROR: "Internal server error",
    TOKEN_PROVIDED: "Token not provided",
    VALIDATE_REQUIRED: "All Component are required fields.",
    SUCCESS: "success",
    FAILED: "failed",
    ERROR: "error",
    INFO: "info",
    SUCCESSFULLY_ADD: "Successfully Add",
    SUCCESSFULLY_UPDATE: "Successfully Update",
    SUCCESSFULLY_DELETE: "Successfully Delete",
    SUCCESSFULLY_SEND: "Successfully Send",
};
