"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mainConfig_1 = __importDefault(require("./configuration/mainConfig"));
const yamlConfig_1 = __importDefault(require("./configuration/yamlConfig"));
const stringConstant_1 = require("../src/constants/stringConstant");
const commonUtils_1 = require("../src/utils/commonUtils");
const app = (0, express_1.default)();
const server = new mainConfig_1.default(app);
app.listen(yamlConfig_1.default.serverPort, 'localhost', function () {
    (0, commonUtils_1.createLog)(stringConstant_1.stringConstant.INFO, stringConstant_1.stringConstant.INFO, `Server is running on port ${yamlConfig_1.default.serverPort}`, 200, "", 0, null);
}).on('error', (err) => {
    if (err.code === 'EADDRINUSE') {
        (0, commonUtils_1.createLog)(stringConstant_1.stringConstant.ERROR, stringConstant_1.stringConstant.ERROR, "server startup error: address already in use", 500, "", err.lineNumber, err);
    }
    else {
        (0, commonUtils_1.createLog)(stringConstant_1.stringConstant.ERROR, stringConstant_1.stringConstant.ERROR, "server failed to start", 500, "", err.lineNumber, err);
    }
});
