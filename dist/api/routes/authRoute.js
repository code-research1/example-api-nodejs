"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const authController_1 = __importDefault(require("../controllers/authController"));
class AuthRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.authCtrl = new authController_1.default();
        this.intializeRoutes();
    }
    intializeRoutes() {
        /**
         * @swagger
         * /login:
         *   post:
         *     summary: authenticate user
         *     description: Takes a create token. Return saved JSON.
         *     tags:
         *       - Authenticate user
         *     consumes:
         *       - application/json
         *     produces:
         *       - application/json
         *     requestBody:
         *       description: JSON object containing resource information.
         *       required: true
         *       content:
         *         application/json:
         *           schema:
         *             type: object
         *             properties:
         *               username:
         *                 type: string
         *               password:
         *                 type: string
         *           example:
         *             username: This is an Example Resource
         *             password: This is an Example Resource
         *     responses:
         *       200:
         *         description: Resource created successfully
         *       400:
         *         description: Bad request
         */
        this.router.route('/').post(this.authCtrl.checkLogin);
    }
}
exports.default = new AuthRoutes().router;
