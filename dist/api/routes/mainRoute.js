"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const yamlConfig_1 = __importDefault(require("../../configuration/yamlConfig"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const swagger_1 = require("../../swagger/swagger");
const authRoute_1 = __importDefault(require("./authRoute"));
const commonRoute_1 = __importDefault(require("./commonRoute"));
const roleRoute_1 = __importDefault(require("./roleRoute"));
const userRoute_1 = __importDefault(require("./userRoute"));
const router = express_1.default.Router();
router.get('/', (req, res) => {
    res.json("Hello, welcome to Web Service " + yamlConfig_1.default.appName);
});
router.use('/docs', swagger_ui_express_1.default.serve);
router.get('/docs', swagger_ui_express_1.default.setup(swagger_1.swaggerSpec));
router.use('/login', authRoute_1.default);
router.use('/common', commonRoute_1.default);
router.use('/role', roleRoute_1.default);
router.use('/user', userRoute_1.default);
exports.default = router;
