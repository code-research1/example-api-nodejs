"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authMiddleware = void 0;
const jwt = __importStar(require("jsonwebtoken"));
const responseConstant_1 = require("../../constants/responseConstant");
const stringConstant_1 = require("../../constants/stringConstant");
const commonUtils_1 = require("../../utils/commonUtils");
const yamlConfig_1 = __importDefault(require("../../configuration/yamlConfig"));
const environment = yamlConfig_1.default.environment;
function authMiddleware(req, res, next) {
    // Get the JWT token from the request headers or cookies
    var _a;
    const token = (_a = req.headers.authorization) === null || _a === void 0 ? void 0 : _a.split(' ')[1];
    const apiKey = req.headers['api-key'];
    const signature = req.headers['signature'];
    const signatureTime = req.headers['signature-time'];
    if (apiKey != yamlConfig_1.default.key[environment].apiKeyEncode) {
        const response = {
            responseCode: responseConstant_1.responseCodes.UNAUTHORIZED,
            responseDescription: responseConstant_1.responseMessages.ERROR,
            responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
            responseDatas: stringConstant_1.stringConstant.HEADER_API_KEY,
        };
        return res.status(401).json(response);
    }
    if (signature != yamlConfig_1.default.key[environment].signatureKeyEncode) {
        const response = {
            responseCode: responseConstant_1.responseCodes.UNAUTHORIZED,
            responseDescription: responseConstant_1.responseMessages.ERROR,
            responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
            responseDatas: stringConstant_1.stringConstant.HEADER_SIGNATURE_KEY,
        };
        return res.status(401).json(response);
    }
    if (Number(signatureTime) <= 0) {
        const response = {
            responseCode: responseConstant_1.responseCodes.UNAUTHORIZED,
            responseDescription: responseConstant_1.responseMessages.ERROR,
            responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
            responseDatas: stringConstant_1.stringConstant.HEADER_API_KEY,
        };
        return res.status(401).json(response);
    }
    if (!token) {
        const response = {
            responseCode: responseConstant_1.responseCodes.UNAUTHORIZED,
            responseDescription: responseConstant_1.responseMessages.ERROR,
            responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
            responseDatas: stringConstant_1.stringConstant.UNAUTHORIZED_ACCESS,
        };
        return res.status(401).json(response);
    }
    try {
        const signature = (0, commonUtils_1.genHMACSHA256)(yamlConfig_1.default.key[environment].signatureKey, yamlConfig_1.default.key[environment].apiKey);
        const decoded = jwt.verify(token, signature);
        // Store the decoded token data in the request for future use
        req.user = decoded;
        next();
    }
    catch (error) {
        if (error.name === 'TokenExpiredError') {
            const response = {
                responseCode: responseConstant_1.responseCodes.UNAUTHORIZED,
                responseDescription: responseConstant_1.responseMessages.ERROR,
                responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                responseDatas: stringConstant_1.stringConstant.PROVIDED_TOKEN_EXPIRED,
            };
            return res.status(401).json(response);
        }
        else if (error.name === 'JsonWebTokenError') {
            const response = {
                responseCode: 403,
                responseDescription: responseConstant_1.responseMessages.ERROR,
                responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                responseDatas: stringConstant_1.stringConstant.TOKEN_DECODING,
            };
            return res.status(403).json(response);
        }
        else {
            const response = {
                responseCode: responseConstant_1.responseCodes.ERROR,
                responseDescription: responseConstant_1.responseMessages.ERROR,
                responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                responseDatas: stringConstant_1.stringConstant.INTERNAL_SERVER_ERROR,
            };
            return res.status(500).json(response);
        }
    }
}
exports.authMiddleware = authMiddleware;
