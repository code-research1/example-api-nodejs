"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModel = void 0;
const sequelize_1 = require("sequelize");
const databaseConfig_1 = require("../../configuration/databaseConfig");
class AuthModel extends sequelize_1.Model {
}
exports.AuthModel = AuthModel;
AuthModel.init({
    idMasterUsers: {
        type: sequelize_1.DataTypes.STRING(40),
        defaultValue: sequelize_1.DataTypes.UUIDV4,
        primaryKey: true,
        unique: true,
        field: 'id_master_users'
    },
    username: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: true,
        field: 'username'
    },
    password: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'password'
    },
    isActive: {
        type: sequelize_1.DataTypes.STRING(20),
        allowNull: false,
        field: 'is_active'
    },
    createdAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: false,
        field: 'created_at',
    },
    updatedAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: false,
        field: 'updated_at',
    },
}, {
    sequelize: databaseConfig_1.sequelize,
    tableName: 'master_users',
});
