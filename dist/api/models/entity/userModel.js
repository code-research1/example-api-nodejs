"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
const sequelize_1 = require("sequelize");
const databaseConfig_1 = require("../../../configuration/databaseConfig");
const roleModel_1 = require("./roleModel");
class UserModel extends sequelize_1.Model {
}
exports.UserModel = UserModel;
UserModel.init({
    idMasterUsers: {
        type: sequelize_1.DataTypes.STRING(40),
        defaultValue: sequelize_1.DataTypes.UUIDV4,
        primaryKey: true,
        unique: true,
        field: 'id_master_users'
    },
    idMasterRoles: {
        type: sequelize_1.DataTypes.STRING(40),
        defaultValue: sequelize_1.DataTypes.UUIDV4,
        field: 'id_master_roles'
    },
    fullname: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'fullname'
    },
    username: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'username'
    },
    isGender: {
        type: sequelize_1.DataTypes.STRING(1),
        allowNull: false,
        field: 'is_gender'
    },
    address: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: true,
        field: 'address'
    },
    hpNumber: {
        type: sequelize_1.DataTypes.STRING(20),
        allowNull: false,
        field: 'hp_number'
    },
    dateActivation: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: true,
        field: 'date_activation'
    },
    email: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: true,
        field: 'email'
    },
    emailVerifiedAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: true,
        field: 'email_verified_at'
    },
    password: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: true,
        field: 'password'
    },
    urlPhoto: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: true,
        field: 'url_photo'
    },
    createdBy: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'created_by'
    },
    updatedBy: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: true,
        field: 'updated_by'
    },
    isActive: {
        type: sequelize_1.DataTypes.STRING(20),
        allowNull: true,
        field: 'is_active'
    },
    createdAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: false,
        field: 'created_at',
    },
    updatedAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: true,
        field: 'updated_at',
    },
}, {
    sequelize: databaseConfig_1.sequelize,
    tableName: 'master_users',
});
UserModel.belongsTo(roleModel_1.RoleModel, {
    foreignKey: 'idMasterRoles', as: 'role'
});
