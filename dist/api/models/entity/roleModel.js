"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleModel = void 0;
const sequelize_1 = require("sequelize");
const databaseConfig_1 = require("../../../configuration/databaseConfig");
class RoleModel extends sequelize_1.Model {
}
exports.RoleModel = RoleModel;
RoleModel.init({
    idMasterRoles: {
        type: sequelize_1.DataTypes.STRING(40),
        defaultValue: sequelize_1.DataTypes.UUIDV4,
        primaryKey: true,
        unique: true,
        field: 'id_master_roles'
    },
    roleName: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'role_name'
    },
    description: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: true,
        field: 'description'
    },
    createdBy: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'created_by'
    },
    updatedBy: {
        type: sequelize_1.DataTypes.STRING(191),
        allowNull: false,
        field: 'updated_by'
    },
    isActive: {
        type: sequelize_1.DataTypes.STRING(20),
        allowNull: false,
        field: 'is_active'
    },
    createdAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: false,
        field: 'created_at',
    },
    updatedAt: {
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.DataTypes.NOW,
        allowNull: true,
        field: 'updated_at',
    },
}, {
    sequelize: databaseConfig_1.sequelize,
    tableName: 'master_roles',
});
// import { Model, Table, Column, DataType } from "sequelize-typescript";
// import { sequelize } from '../../../configuration/databaseConfig';
// @Table({
//   sequelize,
//   tableName: "master_roles",
// })
// export default class RoleModel extends Model {
//     @Column({
//         type: DataType.STRING(40),
//         defaultValue: DataType.UUIDV4,
//         primaryKey: true,
//         unique: true,
//         field: 'id_master_roles'
//     })
//     idMasterRoles?: number;
//     @Column({
//         type: DataType.STRING(191),
//         allowNull: false,
//         field: 'role_name'
//     })
//     roleName?: string;
//     @Column({
//         type: DataType.STRING(191),
//         allowNull: true,
//         field: 'description'
//     })
//     description?: string;
//     @Column({
//         type: DataType.STRING(191),
//         allowNull: false,
//         field: 'created_by'
//     })
//     createdBy?: string;
//     @Column({
//         type: DataType.STRING(191),
//         allowNull: false,
//         field: 'updated_by'
//     })
//     updatedBy?: string;
//     @Column({
//         type: DataType.STRING(20),
//         allowNull: false,
//         field: 'is_active'
//     })
//     isActive?: string;
//     @Column({
//         type: DataType.DATE,
//         defaultValue: DataType.NOW,
//         allowNull: false,
//         field: 'created_at',
//     })
//     createdAt?: Date;
//     @Column({
//         type: DataType.DATE,
//         defaultValue: DataType.NOW,
//         allowNull: true,
//         field: 'updated_at',
//     })
//     updatedAt?: Date;
// }
