"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const authModel_1 = require("../models/entity/authModel");
class AuthRepository {
    constructor() { }
    findOne(param) {
        return authModel_1.AuthModel.findOne({
            where: {
                username: param,
                isActive: "ACTIVED"
            },
        });
    }
}
exports.default = new AuthRepository();
