"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const applicationConstant_1 = require("../../constants/applicationConstant");
const roleModel_1 = require("../models/entity/roleModel");
class RoleRepository {
    constructor() { }
    findAllRoles(req) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let searchCriteria = {};
                let page = parseInt(req.page, 10) || applicationConstant_1.applicationConstant.FIRST_PAGE;
                let limit = parseInt(req.limit, 10) || applicationConstant_1.applicationConstant.PAGINATION_MIN_LIMIT;
                let sortDirection = req.sort_direction || applicationConstant_1.applicationConstant.SORT_DESC;
                let sortBy = req.sort_by || applicationConstant_1.applicationConstant.DEFAULT_SORT_COLOUMN;
                let searchBy = req.search_by;
                let search = req.search;
                let status = req.status;
                if (searchBy.length > 0 && search.length > 0) {
                    if (searchBy == "role_name") {
                        searchCriteria.roleName = { [sequelize_1.Op.iLike]: sequelize_1.Sequelize.fn('LOWER', `%${search}%`) };
                    }
                }
                if (status.length > 0) {
                    searchCriteria.isActive = status;
                }
                const result = yield roleModel_1.RoleModel.findAndCountAll({
                    where: searchCriteria,
                    order: [[sortBy, sortDirection.toUpperCase()]],
                    offset: (page - 1) * limit,
                    limit: limit
                });
                const response = {
                    records: result.rows,
                    totalRecordsCount: result.count,
                };
                return response;
            }
            catch (error) {
                throw new Error("Failed to retrieve Roles!");
            }
        });
    }
    createRoles(datas) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield roleModel_1.RoleModel.create(datas);
            }
            catch (err) {
                throw new Error("Failed to create Role!");
            }
        });
    }
    updateRoles(id, datas) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const affectedRows = yield roleModel_1.RoleModel.update(datas, { where: { idMasterRoles: id.toString() } });
                return affectedRows[0];
            }
            catch (error) {
                throw new Error("Failed to update Role!");
            }
        });
    }
    getDataById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield roleModel_1.RoleModel.findByPk(id.toString());
            }
            catch (error) {
                throw new Error("Failed to retrieve Role!");
            }
        });
    }
}
exports.default = new RoleRepository();
