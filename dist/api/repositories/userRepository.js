"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const userModel_1 = require("../models/entity/userModel");
const roleModel_1 = require("../models/entity/roleModel");
const applicationConstant_1 = require("../../constants/applicationConstant");
const sequelize_1 = require("sequelize");
class UserRepository {
    constructor() { }
    findAllUsers(req) {
        return __awaiter(this, void 0, void 0, function* () {
            let searchCriteria = {};
            let page = parseInt(req.page, 10) || applicationConstant_1.applicationConstant.FIRST_PAGE;
            let limit = parseInt(req.limit, 10) || applicationConstant_1.applicationConstant.PAGINATION_MIN_LIMIT;
            let sortDirection = req.sort_direction || applicationConstant_1.applicationConstant.SORT_DESC;
            let sortBy = req.sort_by || applicationConstant_1.applicationConstant.DEFAULT_SORT_COLOUMN;
            let searchBy = req.search_by;
            let search = req.search;
            let status = req.status;
            if (searchBy.length > 0 && search.length > 0) {
                if (searchBy == "fullname") {
                    searchCriteria.fullname = { [sequelize_1.Op.iLike]: sequelize_1.Sequelize.fn('LOWER', `%${search}%`) };
                }
                if (searchBy == "address") {
                    searchCriteria.address = { [sequelize_1.Op.iLike]: sequelize_1.Sequelize.fn('LOWER', `%${search}%`) };
                }
                if (searchBy == "username") {
                    searchCriteria.username = { [sequelize_1.Op.iLike]: sequelize_1.Sequelize.fn('LOWER', `%${search}%`) };
                }
            }
            if (status.length > 0) {
                searchCriteria.isActive = status;
            }
            const result = yield userModel_1.UserModel.findAndCountAll({
                include: [
                    {
                        model: roleModel_1.RoleModel,
                        as: 'role',
                    }
                ],
                attributes: { exclude: ['password'] },
                where: searchCriteria,
                order: [[sortBy, sortDirection.toUpperCase()]],
                offset: (page - 1) * limit,
                limit: limit
            });
            const response = {
                records: result.rows,
                totalRecordsCount: result.count,
            };
            return response;
        });
    }
    getAllDataUserByParam(param) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield userModel_1.UserModel.findAll({
                where: {
                    email: param,
                },
                attributes: { exclude: ['password'] },
            });
        });
    }
    createUsers(datas) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield userModel_1.UserModel.create(datas);
        });
    }
    updateUsers(id, datas) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield userModel_1.UserModel.update(datas, { where: { idMasterUsers: id.toString() } });
        });
    }
    getDataById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield userModel_1.UserModel.findByPk(id.toString(), {
                attributes: { exclude: ['password', 'age'] },
            });
        });
    }
}
exports.default = new UserRepository();
