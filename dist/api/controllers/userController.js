"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
const userRepository_1 = __importDefault(require("../repositories/userRepository"));
const roleRepository_1 = __importDefault(require("../repositories/roleRepository"));
const responseConstant_1 = require("../../constants/responseConstant");
const commonUtils_1 = require("../../utils/commonUtils");
const stringConstant_1 = require("../../constants/stringConstant");
const bcrypt_1 = __importDefault(require("bcrypt"));
class UserController {
    constructor() { }
    findAll(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { page, limit } = req.query;
                const response = yield userRepository_1.default.findAllUsers(req.query);
                const paginate = {
                    pageNumber: page,
                    pageSize: limit,
                    totalRecordsCount: response.totalRecordsCount,
                    records: response.records,
                };
                const result = {
                    responseCode: responseConstant_1.responseCodes.SUCCESS,
                    responseDescription: responseConstant_1.responseMessages.SUCCESS,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: paginate,
                };
                res.status(200).json(result);
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: "",
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
    getAllDataUserByParam(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const key = req.body.key;
            const value = req.body.value;
            try {
                const datas = yield userRepository_1.default.getAllDataUserByParam(value);
                if (Array.isArray(datas) && datas.length === 0) {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.NOT_FOUND,
                        responseDescription: responseConstant_1.responseMessages.ERROR,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: responseConstant_1.responseMessages.NOT_FOUND,
                    };
                    res.status(200).json(response);
                }
                else {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.SUCCESS,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: "",
                        responseDatas: datas,
                    };
                    res.status(200).json(response);
                }
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: "",
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
    getDataById(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const datas = yield userRepository_1.default.getDataById(req.params.idMasterUsers);
                if (datas == null) {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.NOT_FOUND,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: stringConstant_1.stringConstant.DATA_NOT_FOUND,
                    };
                    res.status(404).json(response);
                }
                else {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.SUCCESS,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: datas,
                    };
                    res.status(200).json(response);
                }
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
    create(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idMasterRoles, fullname, username, isGender, address, hpNumber, email, createdBy } = req.body;
            try {
                const StoreUserCriteria = {
                    idMasterUsers: (0, uuid_1.v4)(),
                    idMasterRoles: idMasterRoles,
                    fullname: fullname,
                    username: username,
                    isGender: isGender,
                    address: address,
                    hpNumber: hpNumber,
                    password: yield bcrypt_1.default.hash("p@ssw0rd", 10),
                    email: email,
                    createdBy: createdBy,
                    isActive: 'ACTIVED',
                };
                const datas = yield userRepository_1.default.createUsers(StoreUserCriteria);
                const response = {
                    responseCode: responseConstant_1.responseCodes.SUCCESS,
                    responseDescription: responseConstant_1.responseMessages.SUCCESS,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: stringConstant_1.stringConstant.SUCCESSFULLY_ADD,
                };
                res.status(200).json(response);
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
    update(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idMasterUsers } = req.params;
            const { idMasterRoles, fullname, username, isGender, address, hpNumber, email, updatedBy } = req.body;
            try {
                const UpdateUserCriteria = {
                    idMasterUsers: idMasterUsers,
                    idMasterRoles: idMasterRoles,
                    fullname: fullname,
                    username: username,
                    isGender: isGender,
                    address: address,
                    hpNumber: hpNumber,
                    email: email,
                    updatedBy: updatedBy,
                    isActive: 'ACTIVED',
                };
                const dataUsers = yield userRepository_1.default.getDataById(idMasterUsers);
                const dataRoles = yield roleRepository_1.default.getDataById(idMasterRoles);
                if (dataUsers == null && dataRoles == null) {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.NOT_FOUND,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: stringConstant_1.stringConstant.DATA_NOT_FOUND,
                    };
                    res.status(404).json(response);
                }
                else {
                    const datas = yield userRepository_1.default.updateUsers(idMasterUsers, UpdateUserCriteria);
                    const response = {
                        responseCode: responseConstant_1.responseCodes.SUCCESS,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: stringConstant_1.stringConstant.SUCCESSFULLY_UPDATE,
                    };
                    res.status(200).json(response);
                }
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
    updateIsActive(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { idMasterUsers } = req.params;
            const { isActive, updatedBy } = req.body;
            try {
                const UpdateIsActiveUserCriteria = {
                    idMasterUsers: idMasterUsers,
                    isActive: isActive,
                    updatedBy: updatedBy,
                };
                const datas = yield userRepository_1.default.getDataById(idMasterUsers);
                if (datas == null) {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.NOT_FOUND,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: stringConstant_1.stringConstant.DATA_NOT_FOUND,
                    };
                    res.status(404).json(response);
                }
                else {
                    const datas = yield userRepository_1.default.updateUsers(idMasterUsers, UpdateIsActiveUserCriteria);
                    const response = {
                        responseCode: responseConstant_1.responseCodes.SUCCESS,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: stringConstant_1.stringConstant.SUCCESSFULLY_DELETE,
                    };
                    res.status(200).json(response);
                }
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
}
exports.default = UserController;
