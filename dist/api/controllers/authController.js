"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = __importStar(require("bcrypt"));
const crypto = __importStar(require("crypto"));
const jwt = __importStar(require("jsonwebtoken"));
const uuid = __importStar(require("uuid"));
const authRepository_1 = __importDefault(require("../repositories/authRepository"));
const responseConstant_1 = require("../../constants/responseConstant");
const stringConstant_1 = require("../../constants/stringConstant");
const commonUtils_1 = require("../../utils/commonUtils");
const yamlConfig_1 = __importDefault(require("../../configuration/yamlConfig"));
const applicationConstant_1 = require("../../constants/applicationConstant");
const environment = yamlConfig_1.default.environment;
class AuthController {
    constructor() { }
    checkLogin(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { username, password } = req.body;
            try {
                const datas = yield authRepository_1.default.findOne(username);
                if (datas == null) {
                    const response = {
                        responseCode: responseConstant_1.responseCodes.NOT_FOUND,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: stringConstant_1.stringConstant.DATA_NOT_FOUND,
                    };
                    res.status(404).json(response);
                }
                else {
                    let constantKey = Buffer.from(applicationConstant_1.applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
                    // Extract IV and ciphertext
                    const ivHex = password.slice(0, 32);
                    const iv = Buffer.from(ivHex, 'hex');
                    const ciphertext = password.slice(32);
                    const keyBuffer = Buffer.from(constantKey, 'hex');
                    const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
                    let decrypted = decipher.update(ciphertext, 'hex', 'utf8');
                    decrypted += decipher.final('utf8');
                    let decryptedPass;
                    try {
                        decryptedPass = JSON.parse(decrypted);
                    }
                    catch (e) {
                        decryptedPass = decrypted;
                    }
                    const passwordIsValid = yield bcrypt.compareSync(decryptedPass, datas.password);
                    if (!passwordIsValid) {
                        const response = {
                            responseCode: responseConstant_1.responseCodes.UNAUTHORIZED,
                            responseDescription: responseConstant_1.responseMessages.ERROR,
                            responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                            responseDatas: stringConstant_1.stringConstant.INVALID_PASSWORD,
                        };
                        res.status(401).json(response);
                    }
                    const payload = {
                        iss: yamlConfig_1.default.key[environment].apiKeyEncode,
                        iat: Math.floor(Date.now() / 1000),
                        exp: Math.floor(Date.now() / 1000) + 86400 * 5,
                        authorized: true,
                        jti: uuid.v4(),
                    };
                    const signatureKey = Buffer.from(yamlConfig_1.default.key[environment].signatureKeyEncode, 'base64').toString('utf-8');
                    const apiKey = Buffer.from(yamlConfig_1.default.key[environment].apiKeyEncode, 'base64').toString('utf-8');
                    const signature = (0, commonUtils_1.genHMACSHA256)(signatureKey, apiKey);
                    const token = jwt.sign(payload, signature, {
                        algorithm: 'HS256',
                    });
                    const response = {
                        responseCode: responseConstant_1.responseCodes.SUCCESS,
                        responseDescription: responseConstant_1.responseMessages.SUCCESS,
                        responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                        responseDatas: token,
                    };
                    res.status(200).json(response);
                }
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
}
exports.default = AuthController;
