"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = __importStar(require("crypto"));
const responseConstant_1 = require("../../constants/responseConstant");
const applicationConstant_1 = require("../../constants/applicationConstant");
const commonUtils_1 = require("../../utils/commonUtils");
class CommonController {
    constructor() { }
    encrypt(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { data, keyType } = req.body;
                let constantKey = "";
                if (keyType === "DATA") {
                    constantKey = Buffer.from(applicationConstant_1.applicationConstant.KEY_AES, 'utf8').toString('hex');
                }
                else if (keyType === "PASSWORD") {
                    constantKey = Buffer.from(applicationConstant_1.applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
                }
                const keyBuffer = Buffer.from(constantKey, 'hex');
                const iv = crypto.randomBytes(16); // 128-bit IV
                // Ensure data is a string (convert it if it's an object or other data type)
                const dataToEncrypt = typeof data === 'string' ? data : JSON.stringify(data);
                const cipher = crypto.createCipheriv('aes-256-cbc', keyBuffer, iv);
                let encryptedData = cipher.update(dataToEncrypt, 'utf8', 'hex');
                encryptedData += cipher.final('hex');
                // Concatenate IV and encrypted data
                const combinedData = iv.toString('hex') + encryptedData;
                const response = {
                    responseCode: responseConstant_1.responseCodes.SUCCESS,
                    responseDescription: responseConstant_1.responseMessages.SUCCESS,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: combinedData,
                };
                res.status(200).json(response);
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
    decrypt(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { encryptedData, keyType } = req.body;
                let constantKey = "";
                if (keyType === "DATA") {
                    constantKey = Buffer.from(applicationConstant_1.applicationConstant.KEY_AES, 'utf8').toString('hex');
                }
                else if (keyType === "PASSWORD") {
                    constantKey = Buffer.from(applicationConstant_1.applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
                }
                // Extract IV and ciphertext
                const ivHex = encryptedData.slice(0, 32);
                const iv = Buffer.from(ivHex, 'hex');
                const ciphertext = encryptedData.slice(32);
                const keyBuffer = Buffer.from(constantKey, 'hex');
                const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
                let decrypted = decipher.update(ciphertext, 'hex', 'utf8');
                decrypted += decipher.final('utf8');
                let decryptedData;
                try {
                    decryptedData = JSON.parse(decrypted);
                }
                catch (e) {
                    decryptedData = decrypted;
                }
                const response = {
                    responseCode: responseConstant_1.responseCodes.SUCCESS,
                    responseDescription: responseConstant_1.responseMessages.SUCCESS,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: decryptedData,
                };
                res.status(200).json(response);
            }
            catch (error) {
                const response = {
                    responseCode: responseConstant_1.responseCodes.ERROR,
                    responseDescription: responseConstant_1.responseMessages.ERROR,
                    responseTime: (0, commonUtils_1.formatTimestamp)(Date.now()),
                    responseDatas: error,
                };
                res.status(500).send(response);
            }
        });
    }
}
exports.default = CommonController;
