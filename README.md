# Express(typescript) Clean Architecture

This project was created to learn nodejs with Express(typescript) framework

## How To Run

1. Run application with command `npm start`

## Feature

- [x] Database ORM
- [x] Database Relational
- [x] Json Validation
- [x] JWT Security
- [x] Open API / Swagger
- [x] Http Client
- [x] Error Handling
- [x] Logging
- [x] Repository Pattern
- [x] Encrypt & Decrypt
- [x] Typescript
- [x] Xtra (Exmple Database & Json)
